\babel@toc {italian}{}
\babel@toc {italian}{}
\contentsline {chapter}{Introduzione}{5}{chapter*.2}
\contentsline {chapter}{\numberline {1}Tecniche algoritmiche per il riconoscimento di immagini}{9}{chapter.1}
\contentsline {section}{\numberline {1.1}Da RGB a Grayscale}{10}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Formato RGB}{10}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Formato Grayscale}{10}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Conversione da RGB a Grayscale}{11}{subsection.1.1.3}
\contentsline {section}{\numberline {1.2}Binarizzazione}{12}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Istogramma}{13}{subsection.1.2.1}
\contentsline {section}{\numberline {1.3}Canny Edge Detection}{17}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Smoothing Gaussiano}{18}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}Calcolo del gradiente}{19}{subsection.1.3.2}
\contentsline {subsection}{\numberline {1.3.3}Soppressione dei non-massimi in direzione\\ ortogonale all\IeC {\textquoteright }edge}{20}{subsection.1.3.3}
\contentsline {subsection}{\numberline {1.3.4}Selezione degli edge significativi mediante\\ isteresi}{22}{subsection.1.3.4}
\contentsline {section}{\numberline {1.4}Segmentazione intera operazione\\matematica}{25}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}Segmentazione orizzontale}{26}{subsection.1.4.1}
\contentsline {subsection}{\numberline {1.4.2}Segmentazione verticale}{30}{subsection.1.4.2}
\contentsline {section}{\numberline {1.5}Mathpix}{31}{section.1.5}
\contentsline {chapter}{\numberline {2}Realt\IeC {\`a} aumentata}{35}{chapter.2}
\contentsline {section}{\numberline {2.1}Definizione}{35}{section.2.1}
\contentsline {section}{\numberline {2.2}Problema delle soglie fisse}{35}{section.2.2}
\contentsline {section}{\numberline {2.3}Rilevamento superficie orizzontale}{36}{section.2.3}
\contentsline {section}{\numberline {2.4}Calcolo della distanza nel mondo reale}{37}{section.2.4}
\contentsline {chapter}{\numberline {3}Aspetti architetturali ed implementativi}{45}{chapter.3}
\contentsline {section}{\numberline {3.1}iOS}{45}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Swift}{45}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Portabilit\IeC {\`a} da iOS ad Android}{46}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Architettura MVC}{47}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Model}{47}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}View}{48}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Controller}{48}{subsection.3.2.3}
\contentsline {section}{\numberline {3.3}Progettazione}{49}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Progettazione algoritmo di segmentazione\\ delle operazioni matematiche}{50}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Progettazione interazione con Mathpix}{51}{subsection.3.3.2}
\contentsline {section}{\numberline {3.4}Implementazione}{52}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Implementazione algoritmo di segmentazione\\ delle operazioni matematiche}{53}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Connessione a Mathpix}{55}{subsection.3.4.2}
\contentsline {section}{\numberline {3.5}Chatbot}{58}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Creazione chatbot}{59}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}Interazione con il chatbot}{59}{subsection.3.5.2}
\contentsline {chapter}{\numberline {4}Conclusioni}{61}{chapter.4}
\contentsline {section}{\numberline {4.1}Lavori futuri}{62}{section.4.1}
\contentsline {chapter}{Bibliografia}{65}{chapter*.25}
