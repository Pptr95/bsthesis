\chapter{Realtà aumentata}

\section{Definizione}
Con realtà aumentata, o dall'inglese Augmented Reality (AR), si vuole descrivere la 
tecnologia che permette la sovrapposizione di livelli informativi alla realtà  
direttamente o indirettamente visualizzata. In altre parole essa può essere descritta
dal concetto di Mixed Reality nel quale realtà e oggetti virtuali vengono combinati in     
tempo reale all'interno di uno spazio tridimensionale.


\section{Problema delle soglie fisse}
L'algoritmo precedente funziona correttamente ma solamente sotto le assunzioni fatte, ovvero che un'operazione deve essere distante da un'altra operazione per almeno una soglia precalcolata e la foto deve essere scattata (senza inquadrare il bordi del foglio) ad una determinata distanza dal foglio, distanza che dipende dalla soglia precalcolata. Quindi ad esempio se la soglia è $x$, per il corretto funzionamento dell'algoritmo, la foto dovrà essere scattata ad una precisa distanza $y$ dal foglio. Se questo vincolo non viene rispettato, si può incorrere nel seguente problema:

\begin{figure}[H]
\centering
\begin{subfigure}{.62\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{Images/Chapter2/distanceNotRespected.jpg}
\end{subfigure}%
\caption{\small{Risultato dell'algoritmo se si scatta la foto senza rispettare il vincolo imposto dalle soglie.}}
\end{figure}

Come si può notare, le due operazioni più a sinistra in Figura 3.1 vengono segmentate in modo anomalo. In particolare l'operazione $12$ $ +$ $3$ $=$ $15$ viene interpretata come due operazioni separate: una operazione composta dal "$12$" ed un'altra operazione composta da "$+ 3 = 15$". Stessa situazione per l'operazione in colonna $1272$ x $40$ $=$ $50800$. E' evidente che il vincolo imposto dalle soglie fisse rende l'algoritmo troppo fragile. 
Si vuole ovviamente che l'algoritmo funzioni correttamente anche scattando foto a distanze differenti. Quindi è necessario un meccanismo che sia in grado di calcolare la distanza che il nostro dispositivo ha dal foglio al momento dello scatto per poi aggiustare di conseguenza la soglia. 
Per risolvere questo problema ci viene in aiuto la realtà aumentata. \\
Per poter fare utilizzo della realtà aumentata si è usufruito della libreria ARKit di Apple.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Rilevamento superficie orizzontale}
Per riuscire a calcolare la distanza nel mondo reale che il dispositivo ha dal foglio instante per instante, è necessario prima di tutto riuscire a fare un rilevamento del piano su cui il foglio a cui scattare la foto è poggiato.
Per riuscire a rilevare i piani orizzontali è necessario configurare opportunamente l'\begin{ttfamily}ARSCNView\end{ttfamily}: una classe che permette di creare, posizionare oggetti aumentati e catturare caratteristiche nel mondo reale.
Riportiamo di seguito la configurazione utilizzata:




\definecolor{lightgray}{rgb}{.9,.9,.9}
\definecolor{darkgray}{rgb}{.4,.4,.4}
\definecolor{purple}{rgb}{0.65, 0.12, 0.82}

\lstdefinelanguage{JavaScript}{
  keywords={typeof, let, private, public, guard, didSet, weak, func, @objc, x, y, z,, view, first, selector, run, super, horizontal, vertical, init, Int, Double, MathpixClient, FormatLatex, FormatWolfram,Float, new, true, false, catch, function, return, null, catch, switch, var, if, in, while, do, else, case, break},
  keywordstyle=\color{purple}\bfseries,
  ndkeywords={class, export, boolean, self, throw, implements, import, this, recognize, on, simplified,  error, result, String, addGestureRecognizer, ARHitTestResult, UITapGestureRecognizer, ARWorldTrackingConfiguration, ARSCNView, viewDidLoad, SCNVector3, ARSCNDebugOptions, OperationGroup, Luminance,SingleComponentGaussianBlur, TextureSamplingOperation, DirectionalSobelEdgeDetectionFragmentShader, TextureSamplingOperation, OneInputVertexShader,TextureSamplingOperation, WeakPixelInclusionFragmentShader DirectionalNonMaximumSuppressionFragmentShader, blurRadiusInPixels, upperThreshold,gaussianBlur, lowerThreshold, luminance, directionalSobel, directionalNonMaximumSuppression, weakPixelInclusion},
  ndkeywordstyle=\color{darkgray}\bfseries,
  identifierstyle=\color{black},
  sensitive=false,
  comment=[l]{//},
  morecomment=[s]{/*}{*/},
  commentstyle=\color{purple}\ttfamily,
  stringstyle=\color{red}\ttfamily,
  morestring=[b]',
  morestring=[b]"
}

\lstset{
   language=JavaScript,
   backgroundcolor=\color{white},
   extendedchars=true,
   basicstyle=\footnotesize\ttfamily,
   showstringspaces=false,
   showspaces=false,
   numbers=left,
   numberstyle=\footnotesize,
   numbersep=9pt,
   tabsize=2,
   breaklines=true,
   showtabs=false,
   captionpos=b
}



\begin{lstlisting}[caption=Configurazione per il rilevamento di superfici orizzontali nel mondo reale.]
    @IBOutlet weak var sceneView: ARSCNView!
    private let configuration = 
    										 ARWorldTrackingConfiguration()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.sceneView.debugOptions = 
        [ARSCNDebugOptions.showWorldOrigin, 
        ARSCNDebugOptions.showFeaturePoints]
        self.configuration.planeDetection = .horizontal
        self.sceneView.session.run(configuration)
        self.sceneView.delegate = self
    }
\end{lstlisting}


L'oggetto \begin{ttfamily}sceneView\end{ttfamily} di tipo \begin{ttfamily}ARSCNView\end{ttfamily} in linea \begin{ttfamily}1\end{ttfamily} rappresenta la classe che permette di creare, posizionare oggetti aumentati e catturare caratteristiche nel mondo reale. In linea \begin{ttfamily}9\end{ttfamily} viene specificato che si vogliono, tra le altre cose, catturare le informazioni relative alle superfici orizzontali. Se si volessero catturare le informazioni anche delle superfici verticali, basterebbe sostituire la riga \begin{ttfamily}9\end{ttfamily} con la seguente riga mostrata di seguito: 

\medskip
\begin{lstlisting}[caption=Configurazione per il rilevamento di superfici orizzontali e verticali nel mondo reale.]
 self.configuration.planeDetection = 
 																[.horizontal, .vertical]
\end{lstlisting}





\section{Calcolo della distanza nel mondo reale}
ARKit ci permette di calcolare la distanza (nel mondo reale) che intercorre fra due nodi oppure fra il dispositivo mobile stesso e un'altro nodo. Un nodo è un elemento strutturale di un grafo di scene, che rappresenta una posizione e si trasforma in uno spazio di coordinate 3D a cui è possibile collegare geometrie, luci, telecamere o altri contenuti visualizzabili. Sapendo che si può fare ciò allora l'idea è stata di posizionare sul foglio (dopo aver fatto il rilevamento del piano orizzontale su cui il foglio poggia) un nodo invisibile all'utente e misurare istante per istante la distanza che intercorreva fra questo nodo e il dispositivo.
Essendo che il posizionamento del nodo invisibile su foglio è stato demandato all'utente tramite un click sullo schermo, è stato necessario rendere lo schermo reattivo ai click. Per fare ciò bisogna aggiungere un \begin{ttfamily}UITapGestureRecognizer\end{ttfamily} alla \begin{ttfamily}sceneView\end{ttfamily}.
Quindi nel codice precedente della \begin{ttfamily}viewDidLoad\end{ttfamily} è necessario aggiungere anche il seguente codice:

\medskip
\begin{lstlisting}[caption=Codice per rendere reattivo lo schermo quando questo viene toccato.]
override func viewDidLoad() {
    ...
    self.registerGestureRecognizerForPlaneAndDistance()
    ...
}
    
private func registerGestureRecognizerForPlaneAndDistance
																										  () {
    let tapGestureRecognizer =
     UITapGestureRecognizer(
       target: self, 
       action: #selector(tappedForPlaceAndDistance))
       self.sceneView.addGestureRecognizer(
       															 tapGestureRecognizer)
}
\end{lstlisting}

Una volta reso lo schermo è reattivo, quando l'utente tocca lo schermo, viene invocato il metodo \begin{ttfamily}tappedForPlaceAndDistance\end{ttfamily}. Questo metodo si occuperà di collocare il nodo invisibile sul foglio. Il codice del metodo \begin{ttfamily}tappedForPlaceAndDistance\end{ttfamily} e dei campi che utilizza è riportato di seguito:



\begin{lstlisting}[caption=Codice per l'aggiunta del nodo invisibile sulla superficie orizzontale rilevata.]
	
    ...
    
    private var startingPosition: SCNNode?
    private var isStartingPositionPlaced = false
    
    ...
    
    @objc private func tappedForPlaceAndDistance(
    										sender: UITapGestureRecognizer) {
        if !self.isStartingPositionPlaced {
            let sceneView = sender.view as! ARSCNView
            let tapLocation = sender.location(
            															in: sceneView)
            let hitTest = 
            		sceneView.hitTest(tapLocation, types: 
            			.existingPlaneUsingExtent)
            if !hitTest.isEmpty {
                addItem(hitTestResult: hitTest.first!)
                self.isStartingPositionPlaced = true
            }
        }
   }
   
   private func addItem(hitTestResult: ARHitTestResult) {
        let transform = hitTestResult.worldTransform
        let thirdCol = transform.columns.3
        let node = SCNNode()
        self.startingPosition = node
        node.position = SCNVector3(thirdCol.x, thirdCol.y, 
        																			 thirdCol.z)
        self.sceneView.scene.rootNode.addChildNode(node)
   }
\end{lstlisting}

Il campo \begin{ttfamily}startingPosition\end{ttfamily} rappresenta il nostro nodo invisibile.
\begin{ttfamily}isStartingPositionPlaced\end{ttfamily} è un campo che tiene traccia del fatto che il nodo invisibile sia stato piazzato o meno.
Quindi quando lo schermo viene toccato, il metodo \begin{ttfamily}tappedForPlaceAndDistance\end{ttfamily} viene invocato: se il nodo non è stato ancora piazzato (riga \begin{ttfamily}10\end{ttfamily}), allora viene presa la posizione nel mondo reale di dove l'utente ha cliccato (riga \begin{ttfamily}12\end{ttfamily}) e a seguire si controlla se l'utente ha cliccato sulla superficie orizzontale rilevata precedentemente (e quindi sul foglio) (riga \begin{ttfamily}13\end{ttfamily}). Se effettivamente l'utente ha cliccato sulla superficie orizzontale rilevata allora si aggiunge il nodo invisibile attraverso il metodo \begin{ttfamily}addItem\end{ttfamily} (righe \begin{ttfamily}16 e 17\end{ttfamily}). Altrimenti se l'utente non ha cliccato sulla superficie orizzontale rilevata, il click sullo schermo viene ignorato.
Una volta piazzato il nodo sul foglio, la seguente procedura entra in funzione e misura la distanza insante per istante dal nodo piazzato al dispositivo:





\begin{lstlisting}[caption=Codice per la misura della distanza dal dispositivo mobile al nodo posizionato sul foglio.]
	
	private func renderer(_ renderer: SCNSceneRenderer, 
									 updateAtTime time: TimeInterval) {
       guard let startingPosition = self.startingPosition 
        																	else {return}
       guard let pointOfView = self.sceneView.pointOfView 
        																	else {return}
       let transform = pointOfView.transform
       let location = 
        	SCNVector3(transform.m41, transform.m42, 
        																	 transform.m43)
       let xDistance = 
       					 location.x - startingPosition.position.x
       let yDistance = 
       					 location.y - startingPosition.position.y
       let zDistance = 
       					 location.z - startingPosition.position.z
       DispatchQueue.main.async {
           return self.distanceTravelled(
           x: xDistance, 
           y: yDistance, 
           z: zDistance)
       }
        
  }
	
  private func distanceTravelled(x: Float, y: Float, 
  																	  z: Float) -> Float {
      return sqrtf(x*x + y*y + z*z)
  }	
	
\end{lstlisting}

Ora che siamo in grado di misurare la distanza che intercorre tra il nostro dispositivo e il foglio, non resta che fare un mapping tra la distanza rilevata e il valore che le soglie devono assumere a quella distanza per il corretto funzionamento dell'algoritmo. Per fare ciò bisogna prima di tutto associare un valore di partenza alle soglie e poi regolarsi su quello. Il valore di partenza è stato scelto tenendo il dispositivo ad una distanza di \begin{ttfamily}26\end{ttfamily} centimetri in modulo dal foglio.
I valori di partenza ricavati a quella distanza dal foglio sono \begin{ttfamily}5\end{ttfamily} per la soglia orizzontale e \begin{ttfamily}7\end{ttfamily} per la soglia verticale. Questo significa che, scattando una foto alla distanza di \begin{ttfamily}26\end{ttfamily} centimetri dal foglio, per una corretta segmentazione delle operazioni è necessario usare come soglia orizzontale il valore \begin{ttfamily}5\end{ttfamily} e come soglia verticale il valore \begin{ttfamily}7\end{ttfamily}, valori che rappresentano la minima distanza che può intercorrere tra la fine di un'intera operazione matematice e l'inzio di un'altra.
È necessario poi calcolare anche i valori che le soglie devono assumere alla distanza minima a cui il dispositivo può scattare la foto. La distanza minima scelta è \begin{ttfamily}4\end{ttfamily} centimetri in modulo dal foglio. A questa distanza i valori per la soglia orizzontale e verticale sono, rispettivamente, \begin{ttfamily}32.50\end{ttfamily} e \begin{ttfamily}45.50\end{ttfamily}. Avendo questi dati riusciamo a calcolare, ad ogni precisa distanza, qual è il valore che le soglie devono assumere.
Di seguito mostriamo una tabella che indica, per distanza in centimetro del dispositivo dal foglio, il valore che assumono le soglie:
\begin{center}
    \begin{tabular}{ | l | l | l | p{5cm} |}
    \hline
    Distanza [cm] & Soglia orizzontale (+1.25) & Soglia verticale (+1.75)\\ \hline
    26 & 5.00 & 7.00  \\ \hline
    25 & 6.25 & 8.75  \\ \hline
    24 & 7.50 & 10.50  \\ \hline
    23 & 8.75 & 12.25  \\ \hline
    22 & 10.00 & 14.00 \\ \hline
    21 & 11.25 & 15.75  \\ \hline
    20 & 12.50 & 17.50  \\ \hline
    19 & 13.75 & 19.25  \\ \hline
    18 & 15.00 & 21.00 \\ \hline
    17 & 16.25 & 22.75  \\ \hline
    16 & 17.50 & 24.50 \\ \hline
    15 & 18.75 & 26.25  \\ \hline
    14 & 20.00 & 28.00  \\ \hline
    13 & 21.25 & 29.75  \\ \hline
    12 & 22.50 & 31.50 \\ \hline
    11 & 23.75 & 33.25 \\ \hline
    10 & 25.00 & 35.00  \\ \hline
    9 & 26.25 & 36.75  \\ \hline
    8 & 27.50 & 38.50  \\ \hline
    7 & 28.75 & 40.25  \\ \hline
    6 & 30.00 & 42.00  \\ \hline
    5 & 31.25 & 43.75  \\ \hline
    4 & 32.50 & 45.50  \\ \hline
    \end{tabular}
\end{center} 


Essendo che le soglie rappresentano un numero di pixel, è stato necessario fare una operazione di approssimazione arrotondando i numeri in virgola mobile all'intero più vicino.\\
Mostriamo di seguito due esempi di funzionamento del seguente algoritmo applicato all'algoritmo di segmentazione delle intere operazioni matematiche: nel primo esempio è stata scattata una foto ad una distanza piuttosto lontana dal foglio (\begin{ttfamily}20\end{ttfamily} centimetri) mentre nel secondo esempio è stata scattata una foto una distanza piuttoto ravvicinata (\begin{ttfamily}6\end{ttfamily} centimetri). \\
Illustriamo il risultato che si otteneva precedentemente senza l'applicazione dell'algoritmo appena descritto e il risultato ottenuto dopo con l'applicazione di questo algoritmo: \\

\Large{\textit{Prima}:}
\begin{figure}[H]
\centering
\begin{subfigure}{.47\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{Images/Chapter2/distanceNotRespected.jpg}
\end{subfigure}%
{\LARGE$\xrightarrow{}$}%
\begin{subfigure}{.50\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{Images/Chapter2/notWorkingAlgorithm.jpg}
 
\end{subfigure}
\caption{\small{Applicazione dell'algoritmo con soglie \textit{fisse} con il dispositivo mobile ad una distanza dal foglio di 20 cm.}}
\end{figure}

\Large{\textit{Dopo}:}
\begin{figure}[H]
\centering
\begin{subfigure}{.49\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{Images/Chapter2/exampleDinamicThreshold.jpg}
\end{subfigure}%
{\LARGE$\xrightarrow{}$}%
\begin{subfigure}{.49\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{Images/Chapter2/finalAlgorithm.jpg}
\end{subfigure}
\caption{\small{Applicazione dell'algoritmo con soglie \textit{dinamiche} con il dispositivo mobile ad una distanza dal foglio di 20 cm.}}
\end{figure}

\Large{\textit{Prima}:}
\begin{figure}[H]
\centering
\begin{subfigure}{.48\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{Images/Chapter2/nearNotWorking.jpg}
\end{subfigure}%
{\LARGE$\xrightarrow{}$}%
\begin{subfigure}{.47\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{Images/Chapter2/outputNearNotWorking.jpg}
 
\end{subfigure}
\caption{\small{Applicazione dell'algoritmo con soglie \textit{fisse} con il dispositivo mobile ad una distanza dal foglio di 6 cm.}}
\end{figure}

\Large{\textit{Dopo}:}
\begin{figure}[H]
\centering
\begin{subfigure}{.48\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{Images/Chapter2/nearWorking.jpg}
\end{subfigure}%
{\LARGE$\xrightarrow{}$}%
\begin{subfigure}{.47\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{Images/Chapter2/outputNearWorking.jpg}
\end{subfigure}
\caption{\small{Applicazione dell'algoritmo con soglie \textit{dinamiche} con il dispositivo mobile ad una distanza dal foglio di 6 cm.}}
\end{figure}

